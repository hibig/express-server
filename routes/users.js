const { createSSRApp } = require('vue')
const { renderToString } = require('@vue/server-renderer')
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', async function (req, res, next) {
  const content = createSSRApp({
    data() {
      return {
        name: 'Tom'
      }
    },
    template: `<div>{{name}} you are come to dream space</div>`
  })
  const htmlString = await renderToString(content)
  const html = ` <html>
  <body>
    <h1>My First Heading</h1>
    <div id="app">${htmlString}</div>
  </body>
</html>`
  res.send(html);
});

module.exports = router;
